defmodule Udp.Collection.Parser do
  use GenServer
  require Logger

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def init(state) do
    start_loop()
    {:ok, state}
  end

  def handle_info(:start_loop, state) do
    time = :os.system_time(:millisecond)
    Logger.info(inspect(parse(Udp.Collection.get_all)))
    start_loop() # Reschedule once more
    Logger.info((:os.system_time(:millisecond) - time) / 1000)
    {:noreply, state}
  end

  defp start_loop() do
    Process.send_after(self(), :start_loop, 10_000) # 10 сек
  end

  def parse([]), do: []
  def parse(collection) do
    # Параллельно выполняем работу над каждым элементом и преобразуем в единый список
    Logger.info(length(collection))
    collection
    |> Stream.take_every(1)
    |> Flow.from_enumerable()
    |> Flow.reduce(fn -> [] end, & json_decode(&1, &2))
    |> Enum.to_list
  end

  def json_decode({:bag, string}, acc) do
    case Poison.decode(string) do
      {:ok, json} -> [json | acc]
      {:error, _} -> acc
    end
  end
  def json_decode(_, acc), do: acc
end
