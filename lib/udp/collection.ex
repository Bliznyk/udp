defmodule Udp.Collection do
  use GenServer
  require Logger

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def init(_opts) do
    :ets.new(:collection, [:public, :duplicate_bag, :named_table])
    {:ok, []}
  end

  def terminate(_reason, _state) do
    # Добавить сохранение текущего стейта в кеш мнезии или redis
  end

  def get_all() do
    GenServer.call(__MODULE__, :get_all)
  end

  def handle_call(:get_all, _from, state) do
    {:reply, do_get_all(:ets.member(:collection, :bag)), state}
  end

  defp do_get_all(true) do
    :ets.take(:collection, :bag)
  end
  defp do_get_all(false), do: []

  def push(value) when is_binary(value) do
    GenServer.cast(__MODULE__, {:push, value})
  end
  def push(value), do: push to_string(value)

  def handle_cast({:push, value}, state) do
    :ets.insert(:collection, {:bag, value})
    {:noreply, state}
  end

end
