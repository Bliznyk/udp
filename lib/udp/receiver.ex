defmodule Udp.Receiver do
  use GenServer
  require Logger

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def init (_opts) do
    {:ok, _socket} = :gen_udp.open(Application.get_env(:udp, :port))
  end

  # Handle UDP data
  def handle_info({:udp, _socket, _ip, _port, data}, state) do
    Udp.Collection.push(data)
    {:noreply, state}
  end

  # Ignore everything else
  def handle_info({_, _socket}, state) do
    {:noreply, state}
  end
end
