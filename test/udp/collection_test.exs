defmodule Udp.CollectionTest do
  use ExUnit.Case, async: true
  alias Udp.Collection

  # setup do
  #   Udp.Collection.start_link
  # end

  test "push" do
    assert Collection.get_all == []
    Collection.push "test"
    Collection.push "foo"
    assert Collection.get_all == ["foo", "test"]
    assert Collection.get_all == []
  end
end
