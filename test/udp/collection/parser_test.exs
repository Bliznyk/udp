defmodule Udp.Collection.ParserTest do
  use ExUnit.Case, async: true
  alias Udp.Collection.Parser

  test "parse collection" do
    tuple = %{"name" => "Bloom", "like" => "Elixir"}
    json = Poison.encode!(tuple)
    parsed_collection = for _ <- 1..1_000_000 do
      tuple
    end
    collection = for _ <- 1..1_000_000 do
      json
    end
    assert Parser.parse(collection) == parsed_collection
  end

  test "dont parse wrong json" do
    tuple = %{"name" => "Bloom", "like" => "Elixir"}
    json = Poison.encode!(tuple)
    wrong_json_string = ":-):-):-):-):-):-):-):-):-):-):-)"
    parsed_collection = for _ <- 1..1_000_000 do
      tuple
    end
    collection = for _ <- 1..1_000_000 do
      json
    end
    assert Parser.parse([wrong_json_string | collection]) == parsed_collection
  end
end
